name := "LanguageTool"

version := "0.4"

scalaVersion := "2.11.7"

mainClass := Some("su.t1001.langtool.Launcher")

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-swing" % "2.11.0-M6",
  "org.xerial" % "sqlite-jdbc" % "3.8.10.2",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "org.slf4j" % "slf4j-simple" % "1.7.12",
  "com.typesafe" % "config" % "1.2.1",
  "org.scalatest" %% "scalatest" % "2.2.5" % "test")
