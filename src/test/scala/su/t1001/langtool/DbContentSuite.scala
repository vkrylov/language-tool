package su.t1001.langtool

import java.sql.{Connection, DriverManager, ResultSet, Statement}

import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory
import su.t1001.langtool.db.{ConnectionSettings, DbConnection}

class DbContentSuite(connection: Connection) {
  private val log = LoggerFactory.getLogger(getClass)

  def fill(): Unit = DbConnection.using(connection.createStatement()) { statement =>
    log info "Start db content suite"
    dropDb(statement)
    createDb(statement)
    fillDb(statement)
    printTestData(statement)
    log info "Db content suite finished"
  }

  private def dropDb(statement: Statement) {
    log info "Drop old tables"
    val dbm = connection.getMetaData
    val tables = Array("EXERCISE", "LOAD", "muscle", "setting", "language", "translate")
    tables.foreach(table => {
      val tables = dbm.getTables(null, null, table.toUpperCase, null)
      log debug s"check table existence: $table"
      if (tables.next()) {
        log warn s"Table $table already exists"
        statement.executeUpdate("DROP TABLE " + table)
      }
    })
  }

  private def createDb(statement: Statement) {
    log info "Create tables"
    statement.executeUpdate(
      """CREATE TABLE EXERCISE(
        | ID INT PRIMARY KEY,
        | CLASSTYPE_ID INT,
        | SFORCETYPE VARCHAR(255),
        | SEQUIPMENT VARCHAR(255),
        | SNAME VARCHAR(255),
        | SDESCRIPTION VARCHAR(255),
        | SINSTRUCTION VARCHAR(255),
        | STIP VARCHAR(255))""".stripMargin)
    statement.executeUpdate(
      """CREATE TABLE LOAD(
        | ID INT PRIMARY KEY,
        | EXERCISE_ID INT,
        | MUSLE_ID INT,
        | PERCENT INT,
        | SDESCRIPTION VARCHAR(255))""".stripMargin)
    statement.executeUpdate(
      """CREATE TABLE muscle(
        | ID INT PRIMARY KEY,
        | SNAME VARCHAR(255),
        | SGROUP VARCHAR(255),
        | SNOTE VARCHAR(255))""".stripMargin)
    statement.executeUpdate(
      """CREATE TABLE setting(
        | NEXTID INT,
        | LANGUAGE_ID INT)""".stripMargin)
    statement.executeUpdate(
      """CREATE TABLE language(
        | ID INT PRIMARY KEY,
        | SNAME VARCHAR(255),
        | SISO2CODE CHAR(2),
        | SISO3CODE CHAR(3),
        | NNUMCODE INT)""".stripMargin)
    statement.executeUpdate(
      """CREATE TABLE translate(
        | OBJECT_ID INT,
        | SFIELD VARCHAR(255),
        | LANGUAGE_ID INT,
        | SMESSAGE VARCHAR(255))""".stripMargin)
  }

  private def fillDb(statement: Statement) {
    log info "Insert table data"
    statement.executeUpdate("INSERT INTO LANGUAGE VALUES(1, 'ENGLISH', 'en', 'eng', '1')")
    statement.executeUpdate("INSERT INTO LANGUAGE VALUES(2, 'RUSSIAN', 'ru', 'rus', '2')")
  }

  private def printTestData(statement: Statement) {
    log info "Check data"
    val r: ResultSet = statement.executeQuery("SELECT * FROM language")
    while (r.next()) {
      val i = r.getInt("ID")
      val s = r.getString("sname")
      log info s"Read row from 'language' = $i $s"
    }
  }
}

object DbContentSuite extends App {
  val settings = ConnectionSettings(ConfigFactory.load())
  new DbContentSuite(DriverManager.getConnection(settings.url, settings.user, settings.url)).fill()
}