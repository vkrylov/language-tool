package su.t1001.langtool

import java.sql.DriverManager

import org.scalatest._
import su.t1001.langtool.db.DbConnection

class DbConnectionTest extends FlatSpec with Matchers {
  behavior of classOf[DbConnection].getSimpleName

  val config = AppConfig.load()
  val settings = config.connectionSettings.copy(url = "jdbc:sqlite::memory:")
  val conn = DriverManager.getConnection(settings.url, settings.user, settings.url)
  new DbContentSuite(conn).fill() should not be Nil
  val connection = DbConnection(conn, config.dbStructure)

  it should "retrieve not empty table list" in {
    connection.tables should not be Nil
  }

  it should "retrieve not empty field list" in {
    connection.fieldNames("LANGUAGE") should not be Nil
  }

  it should "retrieve not empty language list" in {
    connection.languages should not be Nil
  }
}