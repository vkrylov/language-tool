package su.t1001.langtool.db

import com.typesafe.config.Config

object DbStructure {
  def apply(config: Config): DbStructure = {
    val sqlConf = config.getConfig("sql")
    val tablesConf = sqlConf.getConfig("tables")
    val tables = tablesConf.getString("query")
    val tableColName = tablesConf.getString("columnName")
    val fieldsConf = sqlConf.getConfig("fields")
    val fields = fieldsConf.getString("query")
    val fieldColName = fieldsConf.getString("columnName")
    DbStructure(tables, tableColName, fields, fieldColName)
  }
}

case class DbStructure(tables: String, tableColName: String, fields: String, fieldColName: String)
