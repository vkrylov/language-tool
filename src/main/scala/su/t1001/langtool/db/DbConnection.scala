package su.t1001.langtool.db

import java.sql.{Connection, ResultSet, SQLException, Statement}

import org.slf4j.LoggerFactory

import scala.collection.immutable.Nil

object DbConnection {

  def apply(conn: Connection, dbStructure: DbStructure): DbConnection = {
    conn match {
      case null => throw new SQLException("Подключение не создано");
      case con =>
        val res = new DbConnection(con, dbStructure)
        res.languages()
        res
    }
  }

  def using[T <: {def close()}, R](resource: T)(block: T => R): R = {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }
}

class DbConnection(val connection: Connection, dbStructure: DbStructure) {
  private val log = LoggerFactory.getLogger(getClass)

  def tables(): List[String] = useStatement { s =>
    var result: List[String] = Nil
    val r = executeQuery(s, dbStructure.tables)
    while (r.next()) {
      result ::= r.getString(dbStructure.tableColName)
    }
    result
  }

  private def useStatement[T](block: Statement => T): T = {
    DbConnection.using(connection.createStatement()) { s =>
      block(s)
    }
  }

  def fieldNames(table: String): List[String] = useStatement { s =>
    fieldNames(s, table)
  }

  private def fieldNames(statement: Statement, table: String) = {
    var result: List[String] = Nil
    val r = executeQuery(statement, dbStructure.fields.replaceAll("table_name", table))
    while (r.next()) {
      result ::= r.getString(dbStructure.fieldColName)
    }
    result
  }

  def getTableData(table: String) = useStatement { s =>
    val fields = fieldNames(s, table)
    var data: List[Array[Any]] = Nil
    val r = executeQuery(s, s"SELECT * FROM $table")
    while (r.next()) {
      var row: List[Any] = Nil
      fields.foreach(row ::= r.getString(_))
      val aRow: Array[Any] = row.reverse.toArray
      data ::= aRow
    }
    fields -> data.toArray
  }

  def languages() = useStatement { s =>
    val col = "SNAME"
    val r = executeQuery(s, s"SELECT $col FROM LANGUAGE")
    var result: List[String] = Nil
    while (r.next()) {
      result ::= r.getString(col)
    }
    result
  }

  def translation(langName: String, field: String, objId: Long): Option[String] = {
    useStatement { s =>
      val langId = languageId(s, langName)
      val messageField = "SMESSAGE"
      val sql = s"SELECT $messageField FROM TRANSLATE WHERE OBJECT_ID=$objId AND LANGUAGE_ID=$langId AND SFIELD='$field'"
      val r = executeQuery(s, sql)
      if (r.next()) {
        Some(r.getString(messageField))
      } else {
        None
      }
    }
  }

  def save(langName: String, message: String, field: String, objId: Long) {
    useStatement { s =>
      val langId = languageId(s, langName)
      val hasVal = {
        val sql = s"SELECT * FROM TRANSLATE WHERE OBJECT_ID=$objId AND LANGUAGE_ID=$langId AND SFIELD='$field'"
        executeQuery(s, sql).next()
      }
      val sql = if (hasVal) s"UPDATE TRANSLATE SET SMESSAGE='$message' WHERE OBJECT_ID=$objId AND LANGUAGE_ID=$langId AND SFIELD='$field'"
      else s"INSERT INTO TRANSLATE (SMESSAGE, LANGUAGE_ID, SFIELD, OBJECT_ID) VALUES ('$message', $langId, '$field', $objId)"
      executeUpdate(s, sql)
    }
  }

  def languageId(statement: Statement, langName: String) = {
    val r = executeQuery(statement, s"SELECT ID FROM LANGUAGE WHERE SNAME='$langName'")
    r.next()
    r.getLong(1)
  }

  private def executeQuery(statement: Statement, sql: String): ResultSet = {
    log debug s"Execute query: $sql"
    statement executeQuery sql
  }

  private def executeUpdate(statement: Statement, sql: String): Int = {
    log debug s"Execute update: $sql"
    statement executeUpdate sql
  }
}