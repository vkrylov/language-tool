package su.t1001.langtool.db

import com.typesafe.config.Config

object ConnectionSettings {

  def apply(conf: Config): ConnectionSettings = {
    val connectionConf = conf.getConfig("connection")
    val url = connectionConf.getString("url")
    val user = connectionConf.getString("user")
    val password = connectionConf.getString("password")

    ConnectionSettings(url, user, password)
  }
}

case class ConnectionSettings(url: String, user: String, password: String)