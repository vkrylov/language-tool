package su.t1001.langtool.ui

import java.sql.{DriverManager, SQLException}
import javax.swing.{UIManager, WindowConstants}

import org.slf4j.LoggerFactory
import su.t1001.langtool.AppConfig
import su.t1001.langtool.db.DbConnection

import scala.swing._

private[ui] object DbSettingsDialog {
  def apply(mf: MainFrame, settings: AppConfig) = {
    val dbSettings = new DbSettingsDialog(mf, settings)
    dbSettings.open()
    dbSettings.dbConnection()
  }
}

private[ui] class DbSettingsDialog(mf: MainFrame, config: AppConfig) extends Dialog(mf) {
  private val log = LoggerFactory.getLogger(getClass)
  private var connection: Option[DbConnection] = None
  private val connSettings = config.connectionSettings
  val url = new TextField(25)
  url.text = connSettings.url
  val user = new TextField(25)
  user.text = connSettings.user
  val passw = new PasswordField(25)
  passw.text = connSettings.password

  resizable = false
  peer.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)

  val ui = new BoxPanel(Orientation.Vertical) {
    contents += TitledPanels.horizontal("URL", url)
    contents += TitledPanels.horizontal("User", user)
    contents += TitledPanels.horizontal("Password", passw)
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += Button("Подключить") {
        apply()
      }
      contents += Button("Закрыть") {
        sys.exit()
      }
    }
  }

  def apply(): Unit = try {
    val urlText = url.text
    val userText = user.text
    val passwordText = passw.text
    log debug s"Try to connect: $userText@$urlText"
    val conn = DriverManager.getConnection(urlText, userText, passwordText)
    connection = Some(DbConnection(conn, config.dbStructure))
    log debug s"Connected successfully"
    close()
  } catch {
    case e: SQLException =>
      log error (s"Connection failed: ${e.getClass.getName}: ${e.getMessage}", e)
      showErrorDialog(e.getMessage)
  }

  private def showErrorDialog(message: String): Unit = {
    Dialog.showMessage(user, message, "Ошибка", Dialog.Message.Error, UIManager.getIcon("OptionPane.errorIcon"))
  }

  def dbConnection() = connection.get

  modal = true
  title = "Настройки подключнеия к БД"
  preferredSize = new Dimension(300, 200)
  contents = ui
  centerOnScreen()
}