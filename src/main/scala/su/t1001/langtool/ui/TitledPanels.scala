package su.t1001.langtool.ui

import scala.swing.BorderPanel.Position._
import scala.swing._

private[ui] object TitledPanels {

  private def emptyBorder = Swing.EmptyBorder(5, 5, 5, 5)
  
  def vertical(title: String, comp: Component) = new BorderPanel {
    border = emptyBorder
    layout(new Label(title)) = North
    layout(comp) = Center
  }
  
  def horizontal(title: String, comp: Component) = new BorderPanel {
    border = emptyBorder
    layout(new Label(title)) = West
    layout(comp) = East
  }
}