package su.t1001.langtool.ui

import java.awt.Dimension
import javax.swing.UIManager
import javax.swing.table.DefaultTableModel

import org.slf4j.LoggerFactory
import su.t1001.langtool.AppConfig
import su.t1001.langtool.ui.TitledPanels._

import scala.swing.BorderPanel.Position._
import scala.swing._
import scala.swing.event._

class AppFrame(appConfig: AppConfig) extends MainFrame {
  private val log = LoggerFactory.getLogger(getClass)
  val connection = DbSettingsDialog(this, appConfig)
  val contentTable = createTable()
  val tableList = createListView()
  val fieldList = createListView()
  val languageList = createListView()
  val messageTextArea = new TextArea {
    peer.setFont(UIManager.getDefaults.getFont("Label.font"))
  }
  val saveButton = Button("Сохранить") {
    save()
  }
  val ui = new BorderPanel {
    layout(new BoxPanel(Orientation.Vertical) {
      contents += new BorderPanel {
        layout(vertPanel("Таблицы", tableList)) = West
        layout(vertPanel("Содержимое таблиц", contentTable)) = Center
      }
      contents += new BorderPanel {
        layout(new BoxPanel(Orientation.Horizontal) {
          contents += vertPanel("Поля", fieldList)
          contents += vertPanel("Языки", languageList)
        }) = West
        layout(vertPanel("Значение", messageTextArea)) = Center
      }
    }) = Center
    val refreshButton = Button("Обновить") {
      refresh()
    }
    layout(new FlowPanel(FlowPanel.Alignment.Left)(refreshButton, saveButton)) = South
  }

  title = "Language Util"
  preferredSize = new Dimension(600, 400)
  contents = ui
  setListeners()
  refresh()
  centerOnScreen()

  def vertPanel(name: String, c: Component) = vertical(name, new ScrollPane(c))
  
  def save() {
    connection.save(selectedLang.get, messageTextArea.text, selectedField.get, selectedObjectId.get.toLong)
    saveButton.enabled = false
  }

  def selectedObjectId = if (contentTable.selection.rows.nonEmpty) {
    val idColumn = findIdColumn
    if (idColumn.isDefined) {
      Some(contentTable.model.getValueAt(contentTable.selection.rows.leadIndex, idColumn.get).asInstanceOf[String])
    } else {
      log error "Таблица не содержит поля ID"
      None
    }
  } else {
    None
  }

  def hasSelection(listView: ListView[String]) = listView.selection.items.nonEmpty

  def selectedField = optSelected(fieldList)

  def optSelected(list: ListView[String]) = if (hasSelection(list)) {
    Some(list.selection.items.head)
  } else {
    None
  }

  def selectedLang = optSelected(languageList)

  def findIdColumn: Option[Int] = {
    for (i <- 0 until contentTable.model.getColumnCount) {
      contentTable.model.getColumnName(i).toUpperCase match {
        case "ID" => return Some(i)
        case _ =>
      }
    }
    None
  }

  def createTable() = new Table() {
    selection.elementMode = Table.ElementMode.Row
    selection.intervalMode = Table.IntervalMode.Single
  }

  def createListView() = new ListView[String]() {
    selection.intervalMode = ListView.IntervalMode.Single
  }

  def emptyBorder = Swing.EmptyBorder(5, 5, 5, 5)

  def refresh() {
    tableList.listData = connection.tables()
    languageList.listData = connection.languages()
  }

  def setListeners() {
    fieldList listenTo tableList.selection
    fieldList.reactions += {
      case SelectionChanged(`tableList`) if !tableList.selection.adjusting =>
        val items = tableList.selection.items
        fieldList.listData = if (items.nonEmpty) {
          connection.fieldNames(items.head)
        } else {
          Seq.empty[String]
        }
    }

    contentTable listenTo tableList.selection
    contentTable.reactions += {
      case SelectionChanged(`tableList`) if !tableList.selection.adjusting =>
        contentTable.model = if (hasSelection(tableList)) {
          val (headers, rowData) = connection.getTableData(tableList.selection.items.head)
          val model = new DefaultTableModel()
          headers.foreach(model.addColumn)
          rowData.foreach(model addRow _.asInstanceOf[Array[Object]])
          model
        } else {
          new DefaultTableModel()
        }
    }

    messageTextArea.listenTo(tableList.selection, fieldList.selection, languageList.selection, contentTable.selection)
    messageTextArea.reactions += {
      case SelectionChanged(`tableList`) if !tableList.selection.adjusting => changeComponentState()
      case SelectionChanged(`fieldList`) if !fieldList.selection.adjusting => changeComponentState()
      case SelectionChanged(`languageList`) if !languageList.selection.adjusting => changeComponentState()
      case TableRowsSelected(_, _, _) => changeComponentState()
    }

    saveButton listenTo messageTextArea.keys
    saveButton.reactions += {
      case e: KeyReleased => saveButton.enabled = true
    }

    changeComponentState()

    def changeComponentState() {
      def setState(enabled: Boolean, msg: String) {
        saveButton.enabled = false
        saveButton.tooltip = msg
        messageTextArea.enabled = enabled
        messageTextArea.text = enabled match {
          case false => "Отключено"
          case true => connection.translation(selectedLang.get, selectedField.get, selectedObjectId.get.toLong) getOrElse ""
        }
      }

      def disableComponents(msg: String) {
        setState(enabled = false, msg)
      }

      def enableComponents() {
        setState(enabled = true, null)
      }

      selectedField.fold(disableComponents("Не выбрано поле")) { _ =>
        selectedLang.fold(disableComponents("Не выбран язык")) { _ =>
          selectedObjectId.fold(disableComponents("Нет поля с идентификатором объекта")) { _ =>
            enableComponents()
          }
        }
      }
    }
  }
}