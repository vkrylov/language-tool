package su.t1001.langtool

import com.typesafe.config.ConfigFactory
import su.t1001.langtool.db.{ConnectionSettings, DbStructure}

object AppConfig {

  def load(): AppConfig = {
    val appConf = ConfigFactory.load().getConfig("su.t1001.langtool")
    AppConfig(ConnectionSettings(appConf), DbStructure(appConf))
  }
}

case class AppConfig(connectionSettings: ConnectionSettings, dbStructure: DbStructure)
