package su.t1001.langtool

import javax.swing.UIManager

import org.slf4j.LoggerFactory
import su.t1001.langtool.ui.AppFrame

import scala.swing._

object Launcher extends SimpleSwingApplication {
  private val log = LoggerFactory.getLogger(getClass)
  val conf = try {
    AppConfig.load()
  } catch {
    case e: Exception =>
      log error ("Ошибка загрузки конфигурационного файла", e)
      Dialog.showMessage(new Button, "Ошибка загрузки конфигурационного файла\n" + e.getMessage, "Ошибка", Dialog.Message.Error, UIManager.getIcon("OptionPane.errorIcon"))
      sys.exit()
  }

  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

  def top = new AppFrame(conf)
}